'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      room.belongsTo(models.user_games, {
        foreignKey: 'creatorId',
        onDelete: 'CASCADE',
      });
      room.belongsTo(models.user_games, {
        foreignKey: 'targetId',
        onDelete: 'CASCADE',
      });
    }
  }
  room.init(
    {
      result1: DataTypes.STRING,
      result2: DataTypes.STRING,
      result3: DataTypes.STRING,
      choiceCreator1: DataTypes.STRING,
      choiceTarget1: DataTypes.STRING,
      choiceCreator2: DataTypes.STRING,
      choiceTarget2: DataTypes.STRING,
      choiceCreator3: DataTypes.STRING,
      choiceTarget3: DataTypes.STRING,
      posisionGame: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: 'room',
    }
  );
  return room;
};
