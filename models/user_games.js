'use strict';
const { Model } = require('sequelize');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class user_games extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_games.hasOne(models.user_games_biodata, {
        foreignKey: 'userId',
      });
      user_games.hasMany(models.user_games_history, {
        foreignKey: 'userId',
      });
      user_games.hasMany(models.room, {
        foreignKey: 'creatorId',
      });
      user_games.hasMany(models.room, {
        foreignKey: 'targetId',
      });
    }
  }
  user_games.init(
    {
      username: {
        type: DataTypes.STRING,
        validate: {
          len: 5,
          isAlphanumeric: true,
          isUnique: (value, next) => {
            user_games
              .findAll({
                where: { username: value },
              })
              .then((user) => {
                if (user.length != 0) {
                  next(new Error('username already in use'));
                }
                next();
              });
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        validate: {
          is: /^[0-9a-zA-Z]{6}$/i,
        },
        allowNull: false,
      },
      isAdmin: {
        type: DataTypes.BOOLEAN,
      },
    },
    {
      sequelize,
      modelName: 'user_games',
    }
  );

  user_games.addHook('afterValidate', async (user, options) => {
    const salt = await bcrypt.genSaltSync();

    user.password = await bcrypt.hashSync(user.password, salt);
  });

  user_games.prototype.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
  };

  user_games.prototype.generateToken = function () {
    const payload = {
      id: this.id,
      username: this.username,
      isAdmin: this.isAdmin,
    };

    const token = jwt.sign(payload, process.env.JWT_SECRET || 'secret', {
      expiresIn: '1 day',
    });

    return token;
  };

  return user_games;
};
