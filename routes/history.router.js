const historyController = require('../controllers/history.controller');
const webMiddleware = require('../middlewares/web.middleware');

const router = require('express').Router();

router.get(
  '/userGamesHistory',
  webMiddleware.verifyWebSession,
  historyController.index
);
// router.get('/editBiodata/:id', historyController.edit);

module.exports = router;
