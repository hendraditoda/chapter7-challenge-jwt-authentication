const userController = require('../controllers/user.controller');
const webMiddleware = require('../middlewares/web.middleware');

const router = require('express').Router();

// REGION USER GAMES
router.get('/', webMiddleware.verifyWebSession, userController.index);
router.get(
  '/createUserGames',
  webMiddleware.verifyWebSession,
  userController.create
);
router.get('/edit/:id', webMiddleware.verifyWebSession, userController.edit);
router.get('/show/:id', webMiddleware.verifyWebSession, userController.show);
router.get(
  '/show/:id/biodata',
  webMiddleware.verifyWebSession,
  userController.showBiodata
);
router.get(
  '/show/:id/history',
  webMiddleware.verifyWebSession,
  userController.showHistory
);

module.exports = router;
