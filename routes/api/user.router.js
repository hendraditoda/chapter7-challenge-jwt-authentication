const router = require('express').Router();
require('express-group-routes');
const bodyParser = require('body-parser');
const userController = require('../../controllers/api/user.controller');

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

router.post('', urlEncoded, userController.create);
router.post('/edit', urlEncoded, userController.edit);
router.get('/delete/:id', userController.delete);
router.post('/biodata', urlEncoded, userController.addBiodata);
router.post('/history', urlEncoded, userController.addHistory);

module.exports = router;
