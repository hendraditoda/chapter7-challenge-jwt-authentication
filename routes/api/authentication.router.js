const router = require('express').Router();

const bodyParser = require('body-parser');
const authenticationController = require('../../controllers/api/authentication.controller');

const { verifyJWT } = require('../../middlewares/api.middleware');

const urlEncoded = bodyParser.urlencoded({ extended: false });

router.post('/login', urlEncoded, authenticationController.login);
router.post('/register', urlEncoded, authenticationController.register);
router.get('/logout', urlEncoded, authenticationController.logout);

module.exports = router;
