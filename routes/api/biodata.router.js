const router = require('express').Router();

const bodyParser = require('body-parser');
const biodataController = require('../../controllers/api/biodata.controller');

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

router.post('/api/usergamesbiodata/edit', urlEncoded, biodataController.edit);

module.exports = router;
