const authenticationControllerV1 = require('../../../controllers/api/v1/authentication.controller');

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const { verifyJWT } = require('../../../middlewares/api.middleware');

const router = require('express').Router();

router.post('/login', jsonParser, authenticationControllerV1.login);
router.post('/register', jsonParser, authenticationControllerV1.register);
router.get('/verify', jsonParser, verifyJWT, authenticationControllerV1.verify);

module.exports = router;
