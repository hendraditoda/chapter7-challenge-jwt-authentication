const roomControllerV1 = require('../../../controllers/api/v1/room.controller');

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const { verifyJWT } = require('../../../middlewares/api.middleware');

const router = require('express').Router();

router.get('', jsonParser, verifyJWT, roomControllerV1.index);
router.post('/create', jsonParser, verifyJWT, roomControllerV1.create);
router.put('/:id/fight', jsonParser, verifyJWT, roomControllerV1.fight);

module.exports = router;
