const userControllerV1 = require('../../../controllers/api/v1/user.controller');

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const { verifyJWT } = require('../../../middlewares/api.middleware');

const router = require('express').Router();

router.get('', jsonParser, verifyJWT, userControllerV1.index);
router.post('', jsonParser, verifyJWT, userControllerV1.create);
router.put('/edit', jsonParser, verifyJWT, userControllerV1.edit);
router.delete('/delete/:id', jsonParser, verifyJWT, userControllerV1.delete);
router.post('/biodata', jsonParser, verifyJWT, userControllerV1.addBiodata);
router.post('/history', jsonParser, verifyJWT, userControllerV1.addHistory);

module.exports = router;
