const router = require('express').Router();

const homeRouter = require('./home.router');

const userRouter = require('./user.router');
const biodataRouter = require('./biodata.router');
const historyRouter = require('./history.router');

const apiUserRouter = require('./api/user.router');
const apiBiodataRouter = require('./api/biodata.router');
const apiAuthenticationRouter = require('./api/authentication.router');

const apiV1AuthenticationRouter = require('./api/v1/authentication.router');
const apiV1UserRouter = require('./api/v1/user.router');
const apiV1RoomRouter = require('./api/v1/room.router');

// const apiBiodataRouter = require('./api/biodata.router');

// MVC
router.use('/', homeRouter);
router.use('/userGames', userRouter);
router.use(biodataRouter);
router.use(historyRouter);

// MCR
router.use('/api/authentication', apiAuthenticationRouter);
router.use('/api/usergames', apiUserRouter);
router.use(apiBiodataRouter);

//API
router.group('/api/v1', (route) => {
  route.use('/authentication', apiV1AuthenticationRouter);
  route.use('/user', apiV1UserRouter);
  route.use('/room', apiV1RoomRouter);
});

module.exports = router;
