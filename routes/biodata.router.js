const biodataController = require('../controllers/biodata.controller');
const webMiddleware = require('../middlewares/web.middleware');

const router = require('express').Router();

router.get(
  '/userGamesBiodata',
  webMiddleware.verifyWebSession,
  biodataController.index
);
router.get(
  '/editBiodata/:id',
  webMiddleware.verifyWebSession,
  biodataController.edit
);

module.exports = router;
