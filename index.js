const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const Sequelize = require('sequelize');

const SequelizeStore = require('connect-session-sequelize')(session.Store);

const dotenv = require('dotenv');
dotenv.config();

const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/config/config.json')[env];

const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  {
    dialect: config.dialect,
  }
);

sequelize.sync();

const router = require('./routes');

const app = express();

const oneDay = 1000 * 60 * 60 * 24;

app.use(
  session({
    secret: 'secret',
    store: new SequelizeStore({
      db: sequelize,
    }),
    saveUninitialized: true,
    resave: false,
  })
);

app.use(cookieParser());
const port = '3000';

app.set('view engine', 'ejs');

app.use(router);

app.listen(port, () => {
  console.log(`App listening on port: ${port}`);
});
