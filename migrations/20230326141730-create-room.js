'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      creatorId: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'user_games',
          key: 'id',
          as: 'creatorId',
        },
      },
      targetId: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'user_games',
          key: 'id',
          as: 'targetId',
        },
      },
      result1: {
        type: Sequelize.STRING,
      },
      result2: {
        type: Sequelize.STRING,
      },
      result3: {
        type: Sequelize.STRING,
      },
      choiceCreator1: {
        type: Sequelize.STRING,
      },
      choiceTarget1: {
        type: Sequelize.STRING,
      },
      choiceCreator2: {
        type: Sequelize.STRING,
      },
      choiceTarget2: {
        type: Sequelize.STRING,
      },
      choiceCreator3: {
        type: Sequelize.STRING,
      },
      choiceTarget3: {
        type: Sequelize.STRING,
      },
      posisionGame: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('rooms');
  },
};
