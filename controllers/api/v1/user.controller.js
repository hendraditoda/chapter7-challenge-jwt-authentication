const { success } = require('../../../utils/response/json.utils');
const {
  user_games,
  user_games_biodata,
  user_games_history,
} = require('../../../models');

module.exports = {
  index: async (req, res) => {
    let whereQuery = req.user.isAdmin ? {} : { id: req.user.id };

    let orderQuery = [
      ['updatedAt', 'desc'],
      ['createdAt', 'desc'],
    ];

    let includeQuery = [
      { model: user_games_biodata },
      { model: user_games_history },
    ];

    user_games
      .findAll({
        where: whereQuery,
        order: orderQuery,
        include: includeQuery,
      })
      .then((userGames) => {
        return success(res, 200, 'user games', userGames);
      });
  },
  create: async (req, res) => {
    try {
      let userGames = await user_games.create({
        username: req.body.username,
        password: req.body.password,
      });

      return success(res, 201, 'user games created', userGames);
    } catch (err) {
      return error(res, 400, err.message, {});
    }
  },
  edit: async (req, res) => {
    try {
      let userGames = await user_games.findByPk(req.body.id);
      userGames.username = req.body.username
        ? req.body.username
        : userGames.username;
      userGames.password = req.body.password
        ? req.body.password
        : userGames.password;
      userGames.isAdmin = req.body.isAdmin !== undefined;
      await userGames.save();

      return success(res, 201, 'user games edited', userGames);
    } catch (errr) {
      return error(res, 400, err.message, {});
    }
  },
  delete: async (req, res) => {
    try {
      const count = await user_games.destroy({
        where: { id: req.params.id },
      });

      if (count > 0) {
        return success(res, 200, 'user games deleted');
      } else {
        return error(res, 400, 'error deleting profile');
      }
    } catch (err) {
      return error(res, 400, 'error deleting profile');
    }
  },
  addBiodata: async (req, res) => {
    try {
      let userGames = await user_games_biodata.create({
        // userId: req.body.userId,
        userId: req.user.id,
        fullname: req.body.fullname,
        dob: req.body.dob,
        location: req.body.location,
      });

      return success(res, 201, 'user games biodata created', userGames);
    } catch (err) {
      return error(res, 400, err.message, {});
    }
  },
  addHistory: async (req, res) => {
    try {
      let phone = await user_games_history.create({
        // userId: req.body.userId,
        userId: req.user.id,
        game_name: req.body.game_name,
        time_played: req.body.time_played,
        score: req.body.score,
      });

      res.redirect(`/userGames/show/${req.body.userId}`);
    } catch (error) {
      res.render('user_games_history/create', {
        error: error,
        user_games: user_games.findByPk(req.body.userId),
      });
    }
  },
};
