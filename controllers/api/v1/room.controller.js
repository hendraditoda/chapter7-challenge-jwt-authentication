const { success, error } = require('../../../utils/response/json.utils');
const {
  user_games,
  user_games_biodata,
  user_games_history,
  room,
} = require('../../../models');

module.exports = {
  index: async (req, res) => {
    let whereQuery = req.user.isAdmin ? {} : { id: req.user.id };

    let orderQuery = [
      ['updatedAt', 'desc'],
      ['createdAt', 'desc'],
    ];

    let includeQuery = [
      { model: user_games_biodata },
      { model: user_games_history },
    ];

    user_games
      .findAll({
        where: whereQuery,
        order: orderQuery,
        include: includeQuery,
      })
      .then((userGames) => {
        return success(res, 200, 'user games', userGames);
      });
  },
  create: async (req, res) => {
    try {
      let targetUser = await user_games.findOne({
        where: { id: req.body.targetId },
      });

      let Room = await room.create({
        creatorId: req.user.id,
        targetId: targetUser.id,
      });

      return success(res, 201, 'Room berhasil dibuat', { roomId: Room.id });
    } catch (err) {
      return error(res, 400, err.message, {});
    }
  },
  fight: async (req, res) => {
    try {
      let result = null;

      let Room = await room.findByPk(req.params.id);

      if (Room.posisionGame == null) {
        Room.choiceCreator1 = req.body.choiceCreator1
          ? req.body.choiceCreator1
          : Room.choiceCreator1;
        Room.choiceTarget1 = req.body.choiceTarget1
          ? req.body.choiceTarget1
          : Room.choiceTarget1;

        await Room.save();

        if (Room.choiceCreator1 == null && Room.choiceTarget1 == null) {
          return success(res, 201, 'user games updated', {
            choiceCreator1: Room.choiceCreator1,
            choiceTarget1: Room.choiceTarget1,
          });
        }
        if (Room.choiceCreator1 != null && Room.choiceTarget1 != null) {
          if (Room.choiceCreator1 === Room.choiceTarget1) {
            result = null;
          }

          if (Room.choiceCreator1 === 'R' && Room.choiceTarget1 === 'S') {
            result = Room.creatorId;
          }

          if (Room.choiceCreator1 === 'S' && Room.choiceTarget1 === 'P') {
            result = Room.creatorId;
          }

          if (Room.choiceCreator1 === 'P' && Room.choiceTarget1 === 'R') {
            result = Room.creatorId;
          }

          if (Room.choiceCreator1 === 'S' && Room.choiceTarget1 === 'R') {
            result = Room.targetId;
          }

          if (Room.choiceCreator1 === 'P' && Room.choiceTarget1 === 'S') {
            result = Room.targetId;
          }

          if (Room.choiceCreator1 === 'R' && Room.choiceTarget1 === 'P') {
            result = Room.targetId;
          }

          Room.result1 = result;
          Room.posisionGame = true;
          await Room.save();
          return success(res, 201, 'user games win', {
            result1: Room.result1,
          });
        }
      }

      if (Room.posisionGame == true) {
        Room.choiceCreator2 = req.body.choiceCreator2
          ? req.body.choiceCreator2
          : Room.choiceCreator2;
        Room.targetCreator3 = req.body.targetCreator3
          ? req.body.targetCreator3
          : Room.targetCreator3;

        if (Room.choiceCreator2 == null && Room.targetCreator3 == null) {
          return success(res, 201, 'user games updated', {
            choiceCreator2: Room.choiceCreator2,
            targetCreator3: Room.targetCreator3,
          });
        }
        if (Room.choiceCreator2 != null && Room.targetCreator3 != null) {
          if (Room.choiceCreator2 === Room.targetCreator3) {
            result = null;
          }

          if (Room.choiceCreator2 === 'R' && Room.targetCreator3 === 'S') {
            result = Room.creatorId;
          }

          if (Room.choiceCreator2 === 'S' && Room.targetCreator3 === 'P') {
            result = Room.creatorId;
          }

          if (Room.choiceCreator2 === 'P' && Room.targetCreator3 === 'R') {
            result = Room.creatorId;
          }

          if (Room.choiceCreator2 === 'S' && Room.targetCreator3 === 'R') {
            result = Room.targetId;
          }

          if (Room.choiceCreator2 === 'P' && Room.targetCreator3 === 'S') {
            result = Room.targetId;
          }

          if (Room.choiceCreator2 === 'R' && Room.targetCreator3 === 'P') {
            result = Room.targetId;
          }

          Room.result2 = result;

          await Room.save();
          return success(res, 201, 'user games win', {
            result2: Room.result2,
          });
        }
      }

      if (Room.posisionGame == true) {
        Room.choiceCreator3 = req.body.choiceCreator3
          ? req.body.choiceCreator3
          : Room.choiceCreator3;
        Room.targetCreator2 = req.body.targetCreator2
          ? req.body.targetCreator2
          : Room.targetCreator2;

        if (Room.choiceCreator3 == null && Room.targetCreator2 == null) {
          return success(res, 201, 'user games updated', {
            choiceCreator3: Room.choiceCreator3,
            targetCreator2: Room.targetCreator2,
          });
        }
        if (Room.choiceCreator3 != null && Room.targetCreator2 != null) {
          if (Room.choiceCreator3 === Room.targetCreator2) {
            result = null;
          }

          if (Room.choiceCreator3 === 'R' && Room.targetCreator2 === 'S') {
            result = Room.creatorId;
          }

          if (Room.choiceCreator3 === 'S' && Room.targetCreator2 === 'P') {
            result = Room.creatorId;
          }

          if (Room.choiceCreator3 === 'P' && Room.targetCreator2 === 'R') {
            result = Room.creatorId;
          }

          if (Room.choiceCreator3 === 'S' && Room.targetCreator2 === 'R') {
            result = Room.targetId;
          }

          if (Room.choiceCreator3 === 'P' && Room.targetCreator2 === 'S') {
            result = Room.targetId;
          }

          if (Room.choiceCreator3 === 'R' && Room.targetCreator3 === 'P') {
            result = Room.targetId;
          }

          Room.result3 = result;

          await Room.save();
          return success(res, 201, 'user games win', {
            result3: Room.result3,
          });
        }
      }
    } catch (err) {
      return error(res, 400, err.message, {});
    }
  },
};
