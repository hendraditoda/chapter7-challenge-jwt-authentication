const { user_games } = require('../../../models');
const { error, success } = require('../../../utils/response/json.utils');

module.exports = {
  login: async (req, res) => {
    let message = 'Login failed, username/password does not match';

    try {
      const user = await user_games.findOne({
        where: { username: req.body.username },
      });

      //   Password not valid
      if (!user.validPassword(req.body.password)) {
        return error(res, 400, message, {});
      }

      //if login sucess
      return success(res, 200, 'Login sucess', {
        token: user.generateToken(),
      });
    } catch (err) {
      return error(res, 400, err.message, {});
    }
  },
  register: async (req, res) => {
    try {
      let user = await user_games.create({
        username: req.body.username,
        password: req.body.password,
        isAdmin: false,
      });

      //   res.redirect('/login');
      return success(res, 201, 'user created');
    } catch (err) {
      return error(res, 400, err.message, {});
    }
  },
  verify: async (req, res) => {
    return success(res, 200, 'OK');
  },
};
