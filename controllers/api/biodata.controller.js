const {
  user_games,
  user_games_biodata,
  user_games_history,
} = require('../../models');

module.exports = {
  edit: async (req, res) => {
    try {
      let userGamesBiodata = await user_games_biodata.findByPk(req.body.id);
      userGamesBiodata.fullname = req.body.fullname
        ? req.body.fullname
        : userGamesBiodata.fullname;
      userGamesBiodata.dob = req.body.dob ? req.body.dob : userGamesBiodata.dob;
      userGamesBiodata.location = req.body.location
        ? req.body.location
        : userGamesBiodata.location;
      await userGamesBiodata.save();

      res.redirect('/userGamesBiodata');
    } catch (error) {
      res.render('user_games_biodata/edit', {
        error: error,
      });
    }
  },
};
