const {
  user_games,
  user_games_biodata,
  user_games_history,
} = require('../../models');

module.exports = {
  create: async (req, res) => {
    try {
      let userGames = await user_games.create({
        username: req.body.username,
        password: req.body.password,
      });

      res.redirect('/');
    } catch (error) {
      res.render('user_games/create', {
        error: error,
      });
    }
  },
  edit: async (req, res) => {
    try {
      let userGames = await user_games.findByPk(req.body.id);
      userGames.username = req.body.username
        ? req.body.username
        : userGames.username;
      userGames.password = req.body.password
        ? req.body.password
        : userGames.password;
      userGames.isAdmin = req.body.isAdmin !== undefined;
      await userGames.save();

      res.redirect('/');
    } catch (error) {
      res.render('user_games/edit', {
        error: error,
      });
    }
  },
  delete: async (req, res) => {
    try {
      const count = await user_games.destroy({
        where: { id: req.params.id },
      });

      if (count > 0) {
        return res.redirect('/');
      } else {
        return res.redirect('/');
      }
    } catch (err) {
      return res.redirect('/');
    }
  },
  addBiodata: async (req, res) => {
    try {
      let phone = await user_games_biodata.create({
        userId: req.body.userId,
        fullname: req.body.fullname,
        dob: req.body.dob,
        location: req.body.location,
      });

      res.redirect(`/userGames/show/${req.body.userId}`);
    } catch (error) {
      res.render('user_games_biodata/create', {
        error: error,
        user_games: user_games.findByPk(req.body.userId),
      });
    }
  },
  addHistory: async (req, res) => {
    try {
      let phone = await user_games_history.create({
        userId: req.body.userId,
        game_name: req.body.game_name,
        time_played: req.body.time_played,
        score: req.body.score,
      });

      res.redirect(`/userGames/show/${req.body.userId}`);
    } catch (error) {
      res.render('user_games_history/create', {
        error: error,
        user_games: user_games.findByPk(req.body.userId),
      });
    }
  },
};
