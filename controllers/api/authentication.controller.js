const { user_games } = require('../../models');

module.exports = {
  login: async (req, res) => {
    let message = {
      type: 'error',
      message: 'Login failed, username/password does not match',
    };

    try {
      let user = await user_games.findOne({
        where: { username: req.body.username },
      });

      //   Password not valid
      if (!user.validPassword(req.body.password)) {
        return res.render('login', {
          message,
        });
      }

      //   if valid, store to session
      console.log(user.id);
      req.session.userId = user.id;
      req.session.username = user.username;
      req.session.isAdmin = user.isAdmin == true;
      res.redirect('/userGames');
    } catch (error) {
      return res.render('login', {
        message,
      });
    }
  },
  register: async (req, res) => {
    try {
      let user = await user_games.create({
        username: req.body.username,
        password: req.body.password,
        isAdmin: false,
      });

      res.redirect('/login');
    } catch (error) {
      res.render('register', {
        message: {
          type: 'error',
          message: error,
        },
      });
    }
  },
  logout: async (req, res) => {
    if (req.session.userId) {
      req.session.destroy();
      res.redirect('/');
    }
  },
};
