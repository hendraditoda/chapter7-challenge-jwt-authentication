const {
  user_games,
  user_games_biodata,
  user_games_history,
} = require('../models');

module.exports = {
  index: async (req, res) => {
    user_games
      .findAll({
        where: {},
        order: [
          ['updatedAt', 'desc'],
          ['createdAt', 'desc'],
        ],
        include: [{ model: user_games_biodata }, { model: user_games_history }],
      })
      .then((userGames) => {
        res.render('user_games/index', {
          session: req.session,
          userGames: userGames,
        });
      });
  },
  create: async (req, res) => {
    res.render('user_games/create', {
      session: req.session,
    });
  },
  edit: async (req, res) => {
    user_games.findByPk(req.params.id).then((userGames) => {
      res.render('user_games/edit', {
        session: req.session,
        userGames: userGames,
      });
    });
  },
  show: async (req, res) => {
    user_games.findByPk(req.params.id).then((userGames) => {
      res.render('user_games/show', {
        session: req.session,
        userGames: userGames,
      });
    });
  },
  showBiodata: async (req, res) => {
    user_games.findByPk(req.params.id).then((userGames) => {
      res.render('user_games_biodata/create', {
        session: req.session,
        userGames: userGames,
      });
    });
  },
  showHistory: async (req, res) => {
    user_games.findByPk(req.params.id).then((userGames) => {
      res.render('user_games_history/create', {
        session: req.session,
        userGames: userGames,
      });
    });
  },
};
