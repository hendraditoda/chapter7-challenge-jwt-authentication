const {
  user_games,
  user_games_biodata,
  user_games_history,
} = require('../models');

module.exports = {
  index: async (req, res) => {
    let whereQuery = req.session.isAdmin ? {} : { userId: req.session.userId };
    let orderQuery = [
      ['updatedAt', 'desc'],
      ['createdAt', 'desc'],
    ];
    let includeQuery = [{ model: user_games }, { model: user_games_biodata }];
    user_games_history
      .findAll({
        where: whereQuery,
        order: orderQuery,
      })
      .then((userGamesHistory) => {
        res.render('user_games_history/index', {
          session: req.session,
          userGamesHistory: userGamesHistory,
        });
      });
  },
  //   edit: (req, res) => {
  //     user_games_biodata.findByPk(req.params.id).then((userGamesBiodata) => {
  //       res.render('user_games_biodata/edit', {
  //         // session: req.session,
  //         userGamesBiodata: userGamesBiodata,
  //       });
  //     });
  //   },
};
