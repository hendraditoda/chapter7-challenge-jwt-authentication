const {
  user_games,
  user_games_biodata,
  user_games_history,
} = require('../models');

module.exports = {
  index: async (req, res) => {
    let whereQuery = req.session.isAdmin ? {} : { userId: req.session.userId };
    let orderQuery = [
      ['updatedAt', 'desc'],
      ['createdAt', 'desc'],
    ];

    let includeQuery = [{ model: user_games }];

    user_games_biodata
      .findAll({
        where: whereQuery,
        order: orderQuery,
        include: includeQuery,
      })
      .then((userGamesBiodata) => {
        res.render('user_games_biodata/index', {
          session: req.session,
          userGamesBiodata: userGamesBiodata,
        });
      });
  },
  edit: async (req, res) => {
    user_games_biodata.findByPk(req.params.id).then((userGamesBiodata) => {
      res.render('user_games_biodata/edit', {
        session: req.session,
        userGamesBiodata: userGamesBiodata,
      });
    });
  },
};
